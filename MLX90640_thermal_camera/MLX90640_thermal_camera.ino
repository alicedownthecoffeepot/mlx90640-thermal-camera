/*
  Program to read thermal images from a MLX90640 thermal imaging sensor with an Adafruit HUZZAH32 ESP32 Feather board
  and to display a fast raw or slower interpolated thermal video on a 160x128 ST7735 TFT-display.

  Copyright (C) January 2020 by Till Handel under the GNU AGPLv3 license

  For more information see: https://gitlab.com/alicedownthecoffeepot/mlx90640-thermal-camera
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.



  This code is based on:
  - Sparkfun Example3_MaxRefreshRate.ino: "Read the MLX90640 temperature readings as quickly as possible"
    By: Nathan Seidle
    SparkFun Electronics
    Date: May 22nd, 2018
    License: MIT. See license file for more information but you can
    basically do whatever you want with this code.
    https://github.com/sparkfun/SparkFun_MLX90640_Arduino_Example/archive/master.zip
    This relies on the driver written by Melexis and can be found at:
    https://github.com/melexis/mlx90640-library

  - Adafruit ST7738 and ST7789 Library Example "graphicstest"
    Written by Limor Fried/Ladyada for Adafruit Industries.
    MIT license
    https://github.com/adafruit/Adafruit-ST7735-Library/tree/master/examples/graphicstest
  
  
  Hardware:
  - Adafruit HUZZAH32 - ESP32 Feather:
    https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/overview
  - Pimoroni MLX90640 Thermal Camera Breakout – Standard (55°) (connected via I2C)
    https://shop.pimoroni.com/products/mlx90640-thermal-camera-breakout?variant=12536948654163
    (equivalent to https://www.sparkfun.com/products/14844)
  - AZ-Delivery 1.77" 160x128px SPI TFT-display, connected via SPI, equivalent to:
    https://learn.adafruit.com/1-8-tft-display?view=all#breakout-pinouts (ST7735S Blacktab)
  - a 3.7V LiPo rechargeable battery
    
  Library dependencies:
  - Wire (communications)
  - SPI (communication w display)
  - Sparkfun MLX90640 (thermal camera)
  - Adafruit ST7735 TFT (diplay driver)
  - Adafruit GFX (graphics core)
  
  
  Additional links:
  https://learn.adafruit.com/1-8-tft-display?view=all#breakout-pinouts
  https://learn.sparkfun.com/tutorials/qwiic-ir-array-mlx90640-hookup-guide/all
  https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
  https://forums.adafruit.com/viewtopic.php?t=84996
*/


//###########################################
//################ definitions ##############
//###########################################

#include <Wire.h>
#include "MLX90640_API.h"
#include "MLX90640_I2C_Driver.h"

#include <Adafruit_GFX.h>    // Core graphics library
#include <Adafruit_ST7735.h> // Hardware-specific library for ST7735
#include <SPI.h>

//if the drawing is too inefficient try using PDQ libs: PDQ_ST7735, PDQ_FastPin, PDQ_GFX


//################ thermal camera ##################
const byte MLX90640_address = 0x33; //Default 7-bit unshifted address of the MLX90640

#define TA_SHIFT 8 //Default shift for MLX90640 in open air

uint16_t mlx90640Frame[834];
float mlx90640To[768];
paramsMLX90640 mlx90640;


//################ display ##################

// OPTION 1 (recommended) is to use the HARDWARE SPI pins, which are unique
// to each board and not reassignable.
// This is the fastest mode of operation.

// Feather Huzzah32
#define TFT_CS         14
#define TFT_RST        15
#define TFT_DC         32

#define TFT_HEIGHT     160L
#define TFT_WIDTH      128L


#define LED            33
  
// For 1.44" and 1.8" TFT with ST7735 use:
Adafruit_ST7735 tft = Adafruit_ST7735(TFT_CS, TFT_DC, TFT_RST);


#define ROWS 24
#define COLS 32


//###########################################
//################ setup ####################
//###########################################


void setup(){
  pinMode(LED, OUTPUT);

  Wire.begin();
  Wire.setClock(400000); //Increase I2C clock speed to 400kHz - goes easy up to 1MHz

  Serial.begin(115200); //Fast serial as possible - should be supported: 110, 300, 600, 1200, 2400, 4800, 9600, 14400, 19200, 38400, 57600, 115200, 230400, 460800, 921600
  while (!Serial); //Wait for user to open terminal
  
  Serial.println("MLX90640 thermal imager and ST7735 TFT example");
  
  //################ display ##################

  // Use this initializer if using a 1.8" TFT screen:
  tft.initR(INITR_BLACKTAB);      // Init ST7735S chip, black tab

  Serial.println(F("TFT initialized"));

  delay(500);
  
  tft.fillScreen(ST77XX_BLACK);


 
  //################ thermal camera ##################

  if (isConnected() == false){
    Serial.println("MLX90640 not detected at default I2C address. Please check wiring.");
    //while (1); //freeze cpu
  }

  //Get device parameters - We only have to do this once
  int status;
  uint16_t eeMLX90640[832];
  status = MLX90640_DumpEE(MLX90640_address, eeMLX90640);
  if (status != 0)
    Serial.println("Failed to load system parameters");

  status = MLX90640_ExtractParameters(eeMLX90640, &mlx90640);
  if (status != 0)
    Serial.println("Parameter extraction failed");

  //Once params are extracted, we can release eeMLX90640 array

  //Set refresh rate
  //A rate of 0.5Hz takes 4Sec per reading because we have to read two frames to get complete picture
  //MLX90640_SetRefreshRate(MLX90640_address, 0x00); //Set rate to 0.25Hz effective - Works
  //MLX90640_SetRefreshRate(MLX90640_address, 0x01); //Set rate to 0.5Hz effective - Works
  //MLX90640_SetRefreshRate(MLX90640_address, 0x02); //Set rate to 1Hz effective - Works
  //MLX90640_SetRefreshRate(MLX90640_address, 0x03); //Set rate to 2Hz effective - Works
  MLX90640_SetRefreshRate(MLX90640_address, 0x04); //Set rate to 4Hz effective - Works
  //MLX90640_SetRefreshRate(MLX90640_address, 0x05); //Set rate to 8Hz effective - Works at 800kHz
  //MLX90640_SetRefreshRate(MLX90640_address, 0x06); //Set rate to 16Hz effective - Works at 800kHz
  //MLX90640_SetRefreshRate(MLX90640_address, 0x07); //Set rate to 32Hz effective - fails

  //Once EEPROM has been read at 400kHz we can increase to 1MHz
  Wire.setClock(1000000); //may run I2C at 800kHz (because of clock division - not sure)
}



//###########################################
//################ loop #####################
//###########################################


void loop(){
  //Serial.println(xPortGetCoreID()); //print id of core which executes this loop

  for (byte x = 0 ; x < 2 ; x++){  //reading both subpages - however still has to be averaged later

    int status = MLX90640_GetFrameData(MLX90640_address, mlx90640Frame);
  
    digitalWrite(LED, HIGH);
    float vdd = MLX90640_GetVdd(mlx90640Frame, &mlx90640);
    float Ta = MLX90640_GetTa(mlx90640Frame, &mlx90640);
  
    float tr = Ta - TA_SHIFT; //Reflected temperature based on the sensor ambient temperature
    float emissivity = 0.95;
  
    MLX90640_CalculateTo(mlx90640Frame, &mlx90640, emissivity, tr, mlx90640To);
    
    digitalWrite(LED, LOW);
  }

  //enable this to display fast but low-res thermal video
//  drawNormalizedFrame(mlx90640To);

  //enable this to display high-res but slow thermal video
  drawNormalizedInterpFrame(mlx90640To);


}




void drawNormalizedFrame(float data[]){

  static const uint16_t npx = ROWS * COLS;
  uint16_t frame[npx];
  uint16_t c_min = data[0]*100;
  uint16_t c_max = c_min;
  uint16_t tmp;

    
  for(int i=0;i<npx;i++){
    tmp = data[i]*100;
    if(tmp<c_min) c_min = tmp;
    if(tmp>c_max) c_max = tmp;
    frame[i] = tmp;
  }


  for(int i=0; i<npx; i++){
    //map to rainbow color-palette
    frame[i] = scaleAndMapToRainbow(frame[i], c_min, c_max);
  }

  //show temp
  showTempRange(float(c_min)/100, float(c_max)/100);
 


  //draw directly
  static const int pxlsize = 4;
  for(int x=0;x<COLS;x++){
    for(int y=0;y<ROWS;y++){
      tft.fillRect(x*pxlsize,100-y*pxlsize,pxlsize,pxlsize,frame[x+y*COLS]);
    }
  }
 

}


//###########################################
//################ helper funtions ##########
//###########################################


static const uint16_t scale = 4;
static volatile uint16_t aframe[(COLS-1)*scale+1][(ROWS-1)*scale+1];
static volatile uint16_t bframe[(COLS-1)*scale+1][(ROWS-1)*scale+1];
static volatile boolean abswitch = true;



void drawNormalizedInterpFrame(float indata[]){

  const int npx = ROWS * COLS;


  static volatile int frame[npx];
  int c_min = indata[0]*100;
  int c_max = c_min;
  int tmp;

  //determine min and max temperatures of the frame for auto color-scaling and displaying
  for(int i=0;i<npx;i++){
    tmp = indata[i]*100;
    if(tmp<c_min) c_min = tmp;
    if(tmp>c_max) c_max = tmp;
    frame[i] = tmp;
  }

  for(int i=0; i<npx; i++){
    //map to rainbow color-palette
    frame[i] = map(frame[i], c_min, c_max, 0, 1275);
  }

  //switch between 2 frames that are reserved in memory - the not currently used frame will be used to check wich pixels
  //need to be updated on the screen and which don't
  static volatile uint16_t (*iframe)[(ROWS-1)*scale+1];
  static volatile uint16_t (*jframe)[(ROWS-1)*scale+1];

  if(abswitch){
    iframe = aframe;
    jframe = bframe;
  }else{
    iframe = bframe;
    jframe = aframe;
  }


  //hard-coded interpolation filter (i tell myself it's for efficiency reasons :)
  for(int x=0;x<(COLS-1);x++){
    for(int y=0;y<(ROWS-1);y++){

      int corner[4] = {frame[x+y*COLS], frame[x+1+y*COLS], frame[x+(y+1)*COLS], frame[x+1+(y+1)*COLS]};
 
      //corner pixels
      //top left pixel of each 4x4 square
      iframe[x*scale][y*scale] = corner[0];
      
      if(x==(COLS-2)){
        //bottom right pixel in image
        if(y==ROWS-2){
          iframe[(x+1)*scale][(y+1)*scale] = corner[3];
        }
        //end of row
        iframe[(x+1)*scale][y*scale] = corner[1];
        iframe[(x+1)*scale][y*scale+1] = corner[1]*0.75+corner[3]*0.25;
        iframe[(x+1)*scale][y*scale+2] = corner[1]*0.5+corner[3]*0.5;
        iframe[(x+1)*scale][y*scale+3] = corner[1]*0.25+corner[3]*0.75;
      }
      
      //lowest row in image
      if(y==ROWS-2){
        iframe[x*scale][(y+1)*scale] = corner[2];
        iframe[x*scale+1][(y+1)*scale] = corner[2]*0.75+corner[3]*0.25;
        iframe[x*scale+2][(y+1)*scale] = corner[2]*0.5+corner[3]*0.5;
        iframe[x*scale+3][(y+1)*scale] = corner[2]*0.25+corner[3]*0.75;
      }

      
      //row 0
      iframe[x*scale+1][y*scale] = corner[0]*0.75+corner[1]*0.25;
      iframe[x*scale+2][y*scale] = corner[0]*0.5+corner[1]*0.5;
      iframe[x*scale+3][y*scale] = corner[0]*0.25+corner[1]*0.75;

      //row 1
      iframe[x*scale  ][y*scale+1] = corner[0]*0.75+corner[2]*0.25;
      iframe[x*scale+1][y*scale+1] = corner[0]*0.7+corner[1]*0.1+corner[2]*0.1+corner[3]*0.1;
      iframe[x*scale+2][y*scale+1] = corner[0]*0.3+corner[1]*0.3+corner[2]*0.2+corner[3]*0.2;
      iframe[x*scale+3][y*scale+1] = corner[0]*0.1+corner[1]*0.7+corner[2]*0.1+corner[3]*0.1;
      //row 2
      iframe[x*scale  ][y*scale+2] = corner[0]*0.5+corner[2]*0.5;
      iframe[x*scale+1][y*scale+2] = corner[0]*0.3+corner[1]*0.2+corner[2]*0.3+corner[3]*0.2;
      iframe[x*scale+2][y*scale+2] = corner[0]*0.25+corner[1]*0.25+corner[2]*0.25+corner[3]*0.25;
      iframe[x*scale+3][y*scale+2] = corner[0]*0.2+corner[1]*0.3+corner[2]*0.2+corner[3]*0.3;
      //row 3
      iframe[x*scale  ][y*scale+3] = corner[0]*0.25+corner[2]*0.75;
      iframe[x*scale+1][y*scale+3] = corner[0]*0.1+corner[1]*0.1+corner[2]*0.7+corner[3]*0.1;
      iframe[x*scale+2][y*scale+3] = corner[0]*0.2+corner[1]*0.2+corner[2]*0.3+corner[3]*0.3;
      iframe[x*scale+3][y*scale+3] = corner[0]*0.1+corner[1]*0.1+corner[2]*0.1+corner[3]*0.7;
    }
  }

  //show temp
  showTempRange(float(c_min)/100, float(c_max)/100);
  
  
  //map frame to rainbow color palette
  for(int x=0;x<((COLS-1)*4+1);x++){
    for(int y=0;y<((ROWS-1)*4+1);y++){
      iframe[x][y] = mapToRainbow(iframe[x][y]);
    }
  }

  //draw pixels
  for(int x=0;x<((COLS-1)*4+1);x++){
    for(int y=0;y<((ROWS-1)*4+1);y++){
      if(iframe[x][y]!=jframe[x][y]){
        tft.drawPixel(x,100-y,iframe[x][y]);   
      }
    }
  }   
}




uint16_t scaleAndMapToRainbow(uint16_t c_in, const uint16_t c_min, const uint16_t c_max){
    uint16_t tmp = map(c_in, c_min, c_max, 0, 1275);

    if(tmp<256){
      return tft.color565((255-tmp), 0, 255); //violet->blue
    }else if(tmp<511){
      return tft.color565(0, (tmp-255), 255); //blue->cyan
    }else if(tmp<766){
      return tft.color565(0, 255, 765-tmp); //cyan->green
    }else if(tmp<1021){
      return tft.color565(tmp-1022, 255, 0); //green->yellow
    }else{
      return tft.color565(255, 1275-tmp, 0); //yellow->red
    }
}

uint16_t mapToRainbow(uint16_t c_in){
    if(c_in<256){
      return tft.color565((255-c_in), 0, 255); //violet->blue
    }else if(c_in<511){
      return tft.color565(0, (c_in-255), 255); //blue->cyan
    }else if(c_in<766){
      return tft.color565(0, 255, 765-c_in); //cyan->green
    }else if(c_in<1021){
      return tft.color565(c_in-1022, 255, 0); //green->yellow
    }else{
      return tft.color565(255, 1275-c_in, 0); //yellow->red
    }
}

void showTempRange(float t_min, float t_max){
  tft.fillRect(0,110,128,10,ST77XX_BLACK);
  tft.setTextColor(ST77XX_YELLOW);
  tft.setCursor(0,110);
  tft.print(t_min);
  tft.setCursor(85,110);
  tft.print(t_max);
}


//Returns true if the MLX90640 is detected on the I2C bus
boolean isConnected(){
  Wire.beginTransmission((uint8_t)MLX90640_address);
  if (Wire.endTransmission() != 0)
    return (false); //Sensor did not ACK
  return (true);
}

# MLX90640 thermal camera

<img src="/project_documentation/hand.jpg" alt="thermal image of hand"/>


 Program to read thermal images from a MLX90640 thermal imaging sensor with an Adafruit HUZZAH32 ESP32 Feather board
  and to display a fast raw or slower interpolated thermal video on a 160x128 ST7735 TFT-display.

  Copyright (C) January 2020 by Till Handel under the GNU AGPLv3 license

  For more information see: https://gitlab.com/alicedownthecoffeepot/mlx90640-thermal-camera
  
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU Affero General Public License as published
  by the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU Affero General Public License for more details.
  
  You should have received a copy of the GNU Affero General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.



  This code is based on:
  - Sparkfun Example3_MaxRefreshRate.ino: "Read the MLX90640 temperature readings as quickly as possible"
    By: Nathan Seidle
    SparkFun Electronics
    Date: May 22nd, 2018
    License: MIT. See license file for more information but you can
    basically do whatever you want with this code.
    https://github.com/sparkfun/SparkFun_MLX90640_Arduino_Example/archive/master.zip
    This relies on the driver written by Melexis and can be found at:
    https://github.com/melexis/mlx90640-library

  - Adafruit ST7738 and ST7789 Library Example "graphicstest"
    Written by Limor Fried/Ladyada for Adafruit Industries.
    MIT license
    https://github.com/adafruit/Adafruit-ST7735-Library/tree/master/examples/graphicstest
  

    Hardware:
    - Adafruit HUZZAH32 - ESP32 Feather:
      https://learn.adafruit.com/adafruit-huzzah32-esp32-feather/overview
    - Pimoroni MLX90640 Thermal Camera Breakout – Standard (55°) (connected via I2C)
      https://shop.pimoroni.com/products/mlx90640-thermal-camera-breakout?variant=12536948654163
      (equivalent to https://www.sparkfun.com/products/14844)
    - AZ-Delivery 1.77" 160x128px SPI TFT-display, connected via SPI, equivalent to:
      https://learn.adafruit.com/1-8-tft-display?view=all#breakout-pinouts (ST7735S Blacktab)
    - a 3.7V LiPo rechargeable battery

      
    Library dependencies:
    - Wire (communications)
    - SPI (communication w display)
    - Sparkfun MLX90640 (thermal camera)
    - Adafruit ST7735 TFT (diplay driver)
    - Adafruit GFX (graphics core)


    Additional links:
    - https://learn.adafruit.com/1-8-tft-display?view=all#breakout-pinouts
    - https://learn.sparkfun.com/tutorials/qwiic-ir-array-mlx90640-hookup-guide/all
    - https://learn.adafruit.com/adafruit-gfx-graphics-library/graphics-primitives
    - https://forums.adafruit.com/viewtopic.php?t=84996


    Images:
    <img src="project_documentation/hot_mug.jpg" alt="thermal image of a hot mug"/>
    <img src="project_documentation/front.jpg" alt="thermal camera front"/>
    <img src="project_documentation/side.jpg" alt="thermal camera side"/>
    <img src="project_documentation/top.jpg" alt="thermal camera top"/>







